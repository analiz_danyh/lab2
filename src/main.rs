use gnuplot::*;

fn main() {
    let mut x = (2009..=2018).map(f64::from).collect::<Vec<_>>();
    let y = vec![105.4, 85.3, 76.5, 72.2, 69.8, 66.4, 67.3, 65.1, 64.2, 62.1];
    assert_eq!(x.len(), y.len());
    let x_avg = x.iter().sum::<f64>() / x.len() as f64;
    let y_avg = y.iter().sum::<f64>() / y.len() as f64;

    let y_sum = y.iter().sum::<f64>();

    let y_x_min_x_avg = x
        .iter()
        .zip(y.iter())
        .map(|(x, y)| y * (x - x_avg))
        .sum::<f64>();
    let x_min_x_avg_squared = x.iter().map(|v| (v - x_avg).powi(2)).sum::<f64>();
    let y_x_min_x_avg_squared = x
        .iter()
        .zip(y.iter())
        .map(|(x, y)| y * (x - x_avg).powi(2))
        .sum::<f64>();
    let x_min_x_avg_4_pow = x.iter().map(|v| (v - x_avg).powi(4)).sum::<f64>();

    let len = y.len() as f64;

    let a1 = y_x_min_x_avg / x_min_x_avg_squared;

    let a2 = (y_sum / len - y_x_min_x_avg_squared / x_min_x_avg_squared)
        / ((x_min_x_avg_squared / len) - (x_min_x_avg_4_pow / x_min_x_avg_squared));
    let a0 = (y_sum / len) - x_min_x_avg_squared / len * a2;

    dbg!(a0);
    dbg!(a1);
    dbg!(a2);

    let max = y.iter().cloned().fold(std::f64::NEG_INFINITY, f64::max);
    let min = y.iter().cloned().fold(std::f64::INFINITY, f64::min);

    let step = (max - min) / len;
    let mut groups = (0..len as usize)
        .map(|v| (min + v as f64 * step)..(min + (v + 1) as f64 * step))
        .fold(vec![], |mut acc, v| {
            let values = y.iter().filter(|val| v.contains(val)).collect::<Vec<_>>();
            acc.push(values);
            acc
        });
    groups.last_mut().unwrap().push(&max);
    let groups = groups.iter().filter(|v| !v.is_empty()).collect::<Vec<_>>();

    dbg!(groups.clone());
    let m_group_disp = groups
        .iter()
        .map(|&v| {
            let avg = v.iter().cloned().sum::<f64>() / v.len() as f64;
            let squares = (avg - y_avg).powi(2);
            squares * v.len() as f64
        })
        .sum::<f64>()
        / groups.len() as f64;
    dbg!(m_group_disp);

    let avg_group_dispersion = groups
        .iter()
        .map(|&g| {
            let avg = g.iter().cloned().sum::<f64>() / g.len() as f64;
            g.iter().map(|&val| (*val - avg).powi(2)).sum::<f64>()
        })
        .sum::<f64>()
        / y.len() as f64;
    dbg!(avg_group_dispersion);

    let total_dispersion = m_group_disp + avg_group_dispersion;
    dbg!(total_dispersion);

    let r_xy = (m_group_disp / total_dispersion).sqrt();
    let line = x
        .iter()
        .map(|val| a0 + a1 * (val - x_avg) + a2 * (val - x_avg).powi(2))
        .collect::<Vec<_>>();

    let mut fg = Figure::new();
    fg.axes2d()
        .points(
            &x,
            &y,
            &[
                Caption("Actual values"),
                PointSymbol('D'),
                Color("#ffaa77"),
                PointSize(2.0),
            ],
        )
        .lines(&x, &line, &[Caption("Regression"), Color("black")]);
    fg.show();

    assert!(r_xy <= 1.);
    let prefix = if r_xy < 0. {
        "обернений"
    } else {
        "прямий"
    };
    let r_xy_check = r_xy.abs();
    let suffix = if r_xy_check < 0.3 {
        "слабкий"
    } else if r_xy_check < 0.5 {
        "помірний"
    } else if r_xy_check < 0.7 {
        "помітний"
    } else if r_xy_check < 0.9 {
        "високий"
    } else {
        "дуже високий"
    };
    eprintln!(
        "Між показниками {} {} звязок (коефіцієнт = {})",
        prefix, suffix, r_xy
    );

    let s = (line
        .iter()
        .zip(y.iter())
        .map(|(x, y)| (x - y).powi(2))
        .sum::<f64>()
        / line.len() as f64)
        .sqrt();

    let s_squared = (y.iter().map(|v| (v - y_avg).powi(2)).sum::<f64>()) / 8.;
    let delta_a =
        1.363 * s_squared.sqrt() / (x.iter().map(|v| (v - x_avg).powi(2)).sum::<f64>().sqrt());
    let mut suffixes = vec![delta_a; x.len()];
    for y in &[2019., 2020., 2021.] {
        x.extend(&[*y]);

        let x_avg = x.iter().sum::<f64>() / x.len() as f64;
        let x_sum = x.iter().sum::<f64>();
        let s_y = s * ((1. / x.len() as f64) + (x.last().unwrap() - x_avg).powi(2) / x_sum).sqrt();

        suffixes.push(1.363 * s_y);
    }

    let x_avg = x.iter().sum::<f64>() / x.len() as f64;
    let values = x
        .iter()
        .map(|val| a0 + a1 * (val - x_avg) + a2 * (val - x_avg).powi(2))
        .collect::<Vec<_>>();
    let y_n = values.last().unwrap();

    //         t(13-2:0.9) = 1.363
    let suffix = suffixes.last().unwrap();
    let range = y_n - suffix..=y_n + suffix;

    println!("Через 3 роки значення прогнозовано буде {:.2}, а також знаходитись в межах {:.2?} з рівнем значимості 0.9", y_n, range);

    let top_lines = values
        .iter()
        .zip(suffixes.iter())
        .map(|(v, s)| v + s)
        .collect::<Vec<_>>();
    let bottom_lines = values
        .iter()
        .zip(suffixes.iter())
        .map(|(v, s)| v - s)
        .collect::<Vec<_>>();
    let mut fg = Figure::new();
    fg.axes2d()
        .lines(&x, &values, &[Caption("Regression"), Color("black")])
        .lines(
            &x,
            &bottom_lines,
            &[
                Caption("Bottom prediction"),
                Color("red"),
                LineStyle(SmallDot),
            ],
        )
        .lines(
            &x,
            &top_lines,
            &[Caption("Top prediction"), Color("blue"), LineStyle(Dot)],
        );
    fg.show();
}
